package com.degoos.achievements.command.subcommand;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.api.AchievementAPI;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.util.NumericUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AMSubCommandSet extends WSSubcommand {


	public AMSubCommandSet(WSRamifiedCommand command) {
		super("set", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if (remainingArguments.length < 4) {
			return;
		}
		WSPlayer wsPlayer = WetSponge.getServer().getPlayer(remainingArguments[0]).orElse(null);
		if (wsPlayer == null) {
			LanguagesAPI.sendMessage(commandSource, "command.error.playerDoesntExist", Achievements.getInstance());
			return;
		}
		APlayer player = AchievementAPI.getAPlayer(wsPlayer).orElse(null);
		if (player == null) {
			LanguagesAPI.sendMessage(commandSource, "command.error.playerDoesntExist", Achievements.getInstance());
			return;
		}

		if (!NumericUtils.isInteger(remainingArguments[2])) {
			LanguagesAPI.sendMessage(commandSource, "command.error.idMustBeInteger", Achievements.getInstance());
			return;
		}

		Achievement achievement = AchievementAPI.getManager(AchievementManager.class).getAchievement(remainingArguments[1], Integer.valueOf(remainingArguments[2]))
			.orElse(null);

		if (achievement == null) {
			LanguagesAPI.sendMessage(commandSource, "command.error.achievementDoesntExist", Achievements.getInstance());
			return;
		}

		String levelS = remainingArguments[3];

		int level = levelS.equalsIgnoreCase("false") ? 0 : levelS.equalsIgnoreCase("true") ? 1 : NumericUtils.isInteger(levelS) ? Integer.valueOf(levelS) : -1;

		if (level < 0) {
			LanguagesAPI.sendMessage(commandSource, "command.error.invalidLevel", Achievements.getInstance());
			return;
		}

		if (level == 0) player.removeAchievement(achievement);
		else if (achievement instanceof SoloAchievement) player.earnAchievement((SoloAchievement) achievement);
		else if (achievement instanceof ProgressAchievement) player.earnAchievement((ProgressAchievement) achievement, level);
		LanguagesAPI.sendMessage(commandSource, "command.done", Achievements.getInstance());
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		switch (remainingArguments.length) {
			case 1:
				return WetSponge.getServer().getOnlinePlayers().stream().filter(player -> player.getName().toLowerCase().startsWith(remainingArguments[0].toLowerCase()))
					.map(WSPlayer::getName).collect(Collectors.toList());
			case 2:
				return ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().stream()
					.filter(plugin -> plugin.toLowerCase().startsWith(remainingArguments[1].toLowerCase())).collect(Collectors.toList());
			default:
				return new ArrayList<>();
		}
	}
}

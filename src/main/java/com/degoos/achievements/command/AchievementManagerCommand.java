package com.degoos.achievements.command;

import com.degoos.achievements.command.subcommand.AMSubCommandCreate;
import com.degoos.achievements.command.subcommand.AMSubCommandDelete;
import com.degoos.achievements.command.subcommand.AMSubCommandSet;
import com.degoos.achievements.command.subcommand.AMSubcommandHelp;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

public class AchievementManagerCommand extends WSRamifiedCommand {

	public AchievementManagerCommand() {
		super("achievementsManager", "Achievements manager command", null, new String[]{"achm"});
		addSubcommand(new AMSubCommandSet(this));
		addSubcommand(new AMSubCommandCreate(this));
		addSubcommand(new AMSubCommandDelete(this));

		WSSubcommand help = new AMSubcommandHelp(this);
		addSubcommand(help);
		setNotFoundSubcommand(help);
	}

	@Override
	public boolean beforeExecute(WSCommandSource commandSource, String command, String[] arguments) {
		return commandSource.hasPermission("achievements.admin") && super.beforeExecute(commandSource, command, arguments);
	}
}

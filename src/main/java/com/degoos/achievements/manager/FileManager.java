package com.degoos.achievements.manager;

import com.degoos.achievements.Achievements;
import com.degoos.wetsponge.config.ConfigAccessor;
import java.io.File;

public class FileManager implements Manager {

	private ConfigAccessor config;


	public void load() {
		config = new ConfigAccessor(new File(Achievements.getInstance().getDataFolder(), "config.yml"));
		config.checkNodes(new ConfigAccessor(Achievements.getInstance().getResource("achievementsConfig.yml")), true);
	}

	public ConfigAccessor getConfig() {
		return config;
	}
}

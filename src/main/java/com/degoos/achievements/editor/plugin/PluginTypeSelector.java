package com.degoos.achievements.editor.plugin;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.Editor;
import com.degoos.achievements.editor.general.RunnableBack;
import com.degoos.achievements.editor.main.PluginList;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.InventoryRows;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;

public class PluginTypeSelector extends Editor {


	public PluginTypeSelector(WSPlayer player, String plugin) {
		super(LanguagesAPI.getMessage(player, "menu.type.title", false, Achievements.getInstance(), "<PLUGIN>", plugin)
			.orElse(WSText.empty()), InventoryRows.ONE, 1, player, "achievements_type_selector", false);
		WSTask.of(() -> {
			addRunnable(new RunnableProgressAchievements(player, plugin));
			addRunnable(new RunnableSoloAchievements(player, plugin));
			addRunnable(new RunnableBack(player, 8) {
				@Override
				public void onClick(MultiInventoryClickEvent event) {
					new PluginList(player).openFirst();
				}
			});
		}).runAsynchronously(Achievements.getInstance());
	}
}

package com.degoos.achievements.manager;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.listener.LoginListener;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.WSEventManager;

public class ListenerManager implements Manager {

	@Override
	public void load() {
		WSEventManager manager = WetSponge.getEventManager();
		manager.registerListener(new LoginListener(), Achievements.getInstance());
	}
}

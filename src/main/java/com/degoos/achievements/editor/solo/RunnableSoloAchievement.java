package com.degoos.achievements.editor.solo;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorRunnable;
import com.degoos.achievements.manager.MessageManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.itemType.WSItemTypes;
import com.degoos.wetsponge.text.WSText;

public class RunnableSoloAchievement extends EditorRunnable {


	public RunnableSoloAchievement(WSPlayer player, int slot, SoloAchievement achievement) {
		super(player, slot, WSItemStack.of(WSItemTypes.COAL));

		APlayer aPlayer = player.getProperty(APlayer.PROPERTY_NAME, APlayer.class).orElse(null);
		if (aPlayer == null) return;
		boolean hasAchievement = aPlayer.getAchievementPhase(achievement) == 1;
		WSItemStack itemStack = WSItemStack.of(hasAchievement ? WSItemTypes.DIAMOND : WSItemTypes.COAL);
		itemStack.setDisplayName(LanguagesAPI.getMessage(player,
			"menu.challenge.achievement." + (hasAchievement ? "unlockedTitle" : "lockedTitle"), false, Achievements.getInstance(), "<NAME>", achievement.getName())
			.orElse(WSText.empty()));
		itemStack.setLore(MessageManager.getItemSoloDescription(player, achievement, hasAchievement));
		itemStack.setQuantity(Math.min(64, Math.max(achievement.getMaxPoints(), 1)));
		setItemStack(itemStack.update());
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {

	}
}

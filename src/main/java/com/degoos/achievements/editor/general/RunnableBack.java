package com.degoos.achievements.editor.general;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorRunnable;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.HeadDatabase;

public abstract class RunnableBack extends EditorRunnable {

	public RunnableBack(WSPlayer player, int slot) {
		super(player, slot, HeadDatabase.WOOD_LEFT.getHead()
			.setDisplayName(LanguagesAPI.getMessage(player, "menu.general.back", false, Achievements.getInstance()).orElse(WSText.empty())).update());
	}
}

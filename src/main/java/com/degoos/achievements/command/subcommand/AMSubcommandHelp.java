package com.degoos.achievements.command.subcommand;

import com.degoos.achievements.Achievements;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSSuggestCommandAction;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AMSubcommandHelp extends WSSubcommand {

	public AMSubcommandHelp(WSRamifiedCommand command) {
		super("help", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		LanguagesAPI.sendMessage(commandSource, "command.admin.help.header", false, Achievements.getInstance());

		WSText commandText = LanguagesAPI.getMessage(commandSource, "command.admin.help.command", false, Achievements.getInstance()).orElse(WSText.empty());

		getCommand().getSubcommands().stream().sorted(Comparator.comparing(WSSubcommand::getName)).forEach(wsSubcommand -> {
			WSText.Builder builder = commandText
				.replace("<COMMAND>", LanguagesAPI.getMessage(commandSource, "command.admin.help." + wsSubcommand.getName(), false, Achievements.getInstance())
					.orElse(WSText.empty()).toFormattingText()).toBuilder();
			if (commandSource instanceof WSPlayer) {
				builder.hoverAction(WSShowTextAction
					.of(LanguagesAPI.getMessage(commandSource, "command.admin.help." + wsSubcommand.getName() + "HV", false, Achievements.getInstance())
						.orElse(WSText.empty())));
				builder.clickAction(WSSuggestCommandAction.of("/achiAdmin " + wsSubcommand.getName()));
				commandSource.sendMessage(builder.build());
			} else {
				commandSource.sendMessage(builder.build());
				commandSource.sendMessage(LanguagesAPI.getMessage(commandSource, "command.admin.help." + wsSubcommand.getName() + "HV", false, Achievements
					.getInstance())
					.orElse(WSText.empty()));
			}
		});

		LanguagesAPI.sendMessage(commandSource, "command.admin.help.footer", false, Achievements.getInstance());
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}

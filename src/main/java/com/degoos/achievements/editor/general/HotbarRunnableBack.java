package com.degoos.achievements.editor.general;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorHotbarRunnable;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.HeadDatabase;

public abstract class HotbarRunnableBack extends EditorHotbarRunnable {

	public HotbarRunnableBack(WSPlayer player, int slot) {
		super(player, slot, HeadDatabase.WOOD_LEFT.getHead()
			.setDisplayName(LanguagesAPI.getMessage(player, "menu.general.back", false, Achievements.getInstance()).orElse(WSText.empty())).update());
	}
}

package com.degoos.achievements.loader;

import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.manager.DatabaseManager;
import com.degoos.achievements.manager.FileManager;
import com.degoos.achievements.manager.ListenerManager;
import com.degoos.achievements.manager.Manager;
import com.degoos.achievements.manager.MessageManager;
import com.degoos.achievements.manager.PlayerManager;
import com.degoos.achievements.manager.PluginManager;
import java.util.HashMap;
import java.util.Map;

public class ManagerLoader {

	private static Map<Class<?>, Manager> managers;
	private static boolean loaded = false;


	public static void load() {
		managers = new HashMap<>();
		loadDefManagers();
	}


	private static void loadDefManagers() {
		if (loaded) return;
		addManager(new FileManager());
		addManager(new PluginManager());
		addManager(new DatabaseManager());
		addManager(new AchievementManager());
		addManager(new PlayerManager());
		addManager(new MessageManager());
		addManager(new ListenerManager());
		loaded = true;
	}


	public static void addManager(Manager manager) {
		managers.put(manager.getClass(), manager);
		manager.load();
	}


	@SuppressWarnings("unchecked")
	public static <T extends Manager> T getManager(Class<T> clazz) {
		Manager man = managers.get(clazz);
		if (man == null) return null;
		return (T) man;
	}

}

package com.degoos.achievements.manager;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;
import com.degoos.wetsponge.util.NumericUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MessageManager implements Manager {

	@Override
	public void load() {
	}

	public static List<WSText> getAchievementMessage(WSPlayer player, Achievement achievement, int progress) {
		List<WSText> texts;
		texts = LanguagesAPI.getMessages(player, "earnAchievement", false, true, Achievements.getInstance(), "<ACHIEVEMENT>",
			achievement.getName() + (achievement instanceof ProgressAchievement ? " " + NumericUtils.toRomanNumeral(progress) : ""));
		WSText description = getMessageDescription(achievement, progress);
		if (description == null) return texts;
		WSShowTextAction action = WSShowTextAction.of(description);
		return texts.stream().map(text -> text.toBuilder().hoverAction(action).build()).collect(Collectors.toList());
	}

	public static WSText getMessageDescription(Achievement achievement, int progress) {
		String baseDescription = achievement.getDescription().orElse(null);
		if (baseDescription == null) return null;
		String[] words = baseDescription
			.replace("<REQUIRED>", achievement instanceof ProgressAchievement ? String.valueOf(((ProgressAchievement) achievement).getRequired().get(progress)) : "0")
			.split(" ");
		List<String> factorizedDescription = new ArrayList<>();

		StringBuilder currentLine = new StringBuilder();
		int letters = 0;
		for (String word : words) {
			letters += word.length();
			currentLine.append(" ").append(word);
			if (letters > 40) {
				factorizedDescription.add(currentLine.toString().replaceFirst(" ", ""));
				currentLine = new StringBuilder();
				letters = 0;
			}
		}
		if (letters != 0) factorizedDescription.add(currentLine.toString().replaceFirst(" ", ""));

		WSText.Builder description = WSText.builder();
		description.append(WSText.of(achievement.getName(), EnumTextColor.GREEN)).newLine();
		boolean first = true;
		for (String line : factorizedDescription) {
			if (!first) {
				description.newLine();
			} else first = false;
			description.append(WSText.of(line, EnumTextColor.AQUA));
		}
		return description.build();
	}

	public static List<WSText> getItemSoloDescription(WSPlayer player, SoloAchievement achievement, boolean unlocked) {
		String baseDescription = achievement.getDescription().orElse(null);
		if (baseDescription == null) return null;
		String[] words = baseDescription.split(" ");
		List<WSText> description = new ArrayList<>();
		StringBuilder currentLine = new StringBuilder();
		int letters = 0;
		for (String word : words) {
			letters += word.length();
			currentLine.append(" ").append(word);
			if (letters > 25) {
				description.add(WSText.of(currentLine.toString().replaceFirst(" ", ""), EnumTextColor.WHITE));
				currentLine = new StringBuilder();
				letters = 0;
			}
		}
		if (letters != 0) description.add(WSText.of(currentLine.toString().replaceFirst(" ", ""), EnumTextColor.WHITE));

		Object[] replaceList = new Object[]{"<PLUGIN>", achievement.getPlugin(), "<NAME>", achievement.getName(), "<POINTS>", achievement.getMaxPoints(),
			"<LOCKED_MESSAGE>", LanguagesAPI
			.getMessage(player, "menu.challenge.achievement." + (unlocked ? "unlockedMessage" : "lockedMessage"), false, Achievements.getInstance()).orElse(WSText
			.empty())};

		List<WSText> before = LanguagesAPI.getMessages(player, "menu.challenge.achievement.beforeDescriptionLore", false, Achievements.getInstance(), replaceList);
		List<WSText> after = LanguagesAPI.getMessages(player, "menu.challenge.achievement.afterDescriptionLore", false, Achievements.getInstance(), replaceList);
		if (!after.isEmpty() && !(after.size() == 1 && after.get(0).toPlain().equals(""))) description.addAll(after);
		if (before.isEmpty() || (before.size() == 1 && before.get(0).toPlain().equals(""))) return description;
		if (!description.isEmpty() && !(description.size() == 1 && description.get(0).toPlain().equals(""))) before.addAll(description);
		return before;
	}

	public static List<WSText> getItemProgressDescription(WSPlayer player, ProgressAchievement achievement, int phase, boolean unlocked) {
		String baseDescription = achievement.getDescription().orElse(null);
		if (baseDescription == null) return null;
		String[] words = baseDescription.replace("<POINTS>", String.valueOf(achievement.getPoints().getOrDefault(phase, 0))).replace("<PHASE>", String.valueOf(phase))
			.replace("<REQUIRED>", String.valueOf(achievement.getRequired().getOrDefault(phase, 0))).split(" ");
		List<WSText> description = new ArrayList<>();
		StringBuilder currentLine = new StringBuilder();
		int letters = 0;
		for (String word : words) {
			letters += word.length();
			currentLine.append(" ").append(word);
			if (letters > 25) {
				description.add(WSText.of(currentLine.toString().replaceFirst(" ", ""), EnumTextColor.WHITE));
				currentLine = new StringBuilder();
				letters = 0;
			}
		}
		if (letters != 0) description.add(WSText.of(currentLine.toString().replaceFirst(" ", ""), EnumTextColor.WHITE));

		Object[] replaceList = new Object[]{"<PLUGIN>", achievement.getPlugin(), "<NAME>", achievement.getName(), "<PHASE>", NumericUtils.toRomanNumeral(phase),
			"<POINTS>", achievement.getPoints().get(phase), "<REQUIRED>", achievement.getRequired().getOrDefault(phase, 0), "<LOCKED_MESSAGE>", LanguagesAPI
			.getMessage(player, "menu.tiered.achievement." + (unlocked ? "unlockedMessage" : "lockedMessage"), false, Achievements.getInstance()).orElse(WSText.empty
			())};

		List<WSText> before = LanguagesAPI.getMessages(player, "menu.tiered.achievement.beforeDescriptionLore", false, Achievements.getInstance(), replaceList);
		List<WSText> after = LanguagesAPI.getMessages(player, "menu.tiered.achievement.afterDescriptionLore", false, Achievements.getInstance(), replaceList);
		if (!after.isEmpty() && !(after.size() == 1 && after.get(0).toPlain().equals(""))) description.addAll(after);
		if (before.isEmpty() || (before.size() == 1 && before.get(0).toPlain().equals(""))) return description;
		if (!description.isEmpty() && !(description.size() == 1 && description.get(0).toPlain().equals(""))) before.addAll(description);
		return before;
	}
}

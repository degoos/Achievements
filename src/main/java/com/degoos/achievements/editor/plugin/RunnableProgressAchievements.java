package com.degoos.achievements.editor.plugin;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorRunnable;
import com.degoos.achievements.editor.progress.ProgressList;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.material.itemType.WSItemTypes;
import com.degoos.wetsponge.sound.WSSound;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;
import java.util.stream.Collectors;

public class RunnableProgressAchievements extends EditorRunnable {

	private Set<ProgressAchievement> achievements;
	private String plugin;


	public RunnableProgressAchievements(WSPlayer player, String plugin) {
		super(player, 6, WSItemStack.of(WSItemTypes.DIAMOND));
		this.plugin = plugin;

		APlayer aPlayer = player.getProperty(APlayer.PROPERTY_NAME, APlayer.class).orElse(null);
		if (aPlayer == null) return;

		AchievementManager manager = ManagerLoader.getManager(AchievementManager.class);
		achievements = manager.getAchievements(plugin).stream().filter(achievement -> achievement instanceof ProgressAchievement).map(a -> (ProgressAchievement) a)
			.collect(Collectors.toSet());

		int maxAchievements = achievements.stream().mapToInt(achievement -> ((ProgressAchievement) achievement).getRequiredLength()).sum();
		int unlocked = achievements.stream().mapToInt(aPlayer::getAchievementPhase).sum();

		int maxPoints = achievements.stream().mapToInt(Achievement::getMaxPoints).sum();
		int points = achievements.stream().mapToInt(achievement -> achievement.getPoints(aPlayer)).sum();

		int unlockPer = maxAchievements == 0 ? 100 : unlocked * 100 / maxAchievements;
		int pointsPer = maxPoints == 0 ? 100 : points * 100 / maxPoints;

		StringBuilder greenBar = new StringBuilder();
		StringBuilder grayBar = new StringBuilder();
		for (int i = 5; i <= 100; i += 5) {
			if (i <= unlockPer) greenBar.append("■");
			else grayBar.append("■");
		}
		WSText unlockBar = WSText.of(greenBar.toString(), EnumTextColor.GREEN, WSText.of(grayBar.toString(), EnumTextColor.DARK_GRAY));

		greenBar = new StringBuilder();
		grayBar = new StringBuilder();
		for (int i = 5; i <= 100; i += 5) {
			if (i <= pointsPer) greenBar.append("■");
			else grayBar.append("■");
		}
		WSText pointsBar = WSText.of(greenBar.toString(), EnumTextColor.GREEN, WSText.of(grayBar.toString(), EnumTextColor.DARK_GRAY));

		WSItemStack item = WSItemStack.of(WSBlockTypes.DIAMOND_BLOCK);
		item.setDisplayName(LanguagesAPI.getMessage(player, "menu.type.tiered.title", false, Achievements.getInstance(), "<PLUGIN>", plugin).orElse(WSText.empty()));
		item.setLore(LanguagesAPI.getMessages(player, "menu.type.tiered.lore", false, Achievements
			.getInstance(), "<PLUGIN>", plugin, "<UNLOCKED>", unlocked, "<ACHIEVEMENTS>", maxAchievements, "<UNLOCKED_PER>", unlockPer, "<UNLOCK_BAR>", unlockBar,
			"<POINTS>", points, "<MAX_POINTS>", maxPoints, "<POINTS_PER>", pointsPer, "<POINTS_BAR>", pointsBar));

		item.update();
		setItemStack(item);
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {
		event.getPlayer().playSound(WSSound.UI_BUTTON_CLICK, 1);
		new ProgressList(getPlayer(), plugin, achievements).openFirst();
	}
}

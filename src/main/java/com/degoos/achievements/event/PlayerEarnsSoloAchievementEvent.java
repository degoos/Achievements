package com.degoos.achievements.event;

import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.util.Validate;

public class PlayerEarnsSoloAchievementEvent extends WSEvent {

	private APlayer aPlayer;
	private SoloAchievement achievement;

	public PlayerEarnsSoloAchievementEvent(APlayer aPlayer, SoloAchievement achievement) {
		Validate.notNull(aPlayer, "APlayer cannot be null!");
		Validate.notNull(achievement, "Achievement cannot be null!");
		this.aPlayer = aPlayer;
		this.achievement = achievement;
	}

	public APlayer getPlayer() {
		return aPlayer;
	}

	public SoloAchievement getAchievement() {
		return achievement;
	}


	public static class Pre extends PlayerEarnsSoloAchievementEvent implements WSCancellable {

		private boolean cancelled;

		public Pre(APlayer aPlayer, SoloAchievement achievement) {
			super(aPlayer, achievement);
			cancelled = false;
		}

		public void setAchievement(SoloAchievement achievement) {
			Validate.notNull(achievement, "Achievement cannot be null!");
			super.achievement = achievement;
		}

		@Override
		public boolean isCancelled() {
			return cancelled;
		}

		@Override
		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}
	}

	public static class Post extends PlayerEarnsSoloAchievementEvent {

		public Post(APlayer aPlayer, SoloAchievement achievement) {
			super(aPlayer, achievement);
		}
	}
}

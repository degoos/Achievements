package com.degoos.achievements.editor;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickHotbarEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;

public abstract class EditorHotbarRunnable extends EditorRunnable {

	public EditorHotbarRunnable(WSPlayer player, int slot, WSItemStack itemStack) {
		super(player, slot, itemStack);
	}

	@Override
	public void setItemStack(WSItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		setItemStackWithoutUpdating(itemStack);
		if (getEditor() != null) getEditor().setItemOnHotbar(super.getSlot(), itemStack);
	}

	public void onClick(MultiInventoryClickEvent event) {

	}

	public abstract void onHotbarClick(MultiInventoryClickHotbarEvent event);

}

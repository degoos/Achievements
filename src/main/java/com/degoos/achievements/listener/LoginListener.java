package com.degoos.achievements.listener;

import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.PlayerManager;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;

public class LoginListener {

	@WSListener
	public void onJoin(WSPlayerJoinEvent event) {
		ManagerLoader.getManager(PlayerManager.class).addPlayer(event.getPlayer());
	}
}

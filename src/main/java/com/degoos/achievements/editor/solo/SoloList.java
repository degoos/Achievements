package com.degoos.achievements.editor.solo;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.Editor;
import com.degoos.achievements.editor.general.HotbarRunnableBack;
import com.degoos.achievements.editor.main.PluginList;
import com.degoos.achievements.editor.plugin.PluginTypeSelector;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.InventoryRows;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickHotbarEvent;
import com.degoos.wetsponge.sound.WSSound;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;
import java.util.stream.Collectors;

public class SoloList extends Editor {

	private static final int[] slots = new int[]{10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25, 28, 29, 30, 31, 32, 33, 34};

	public SoloList(WSPlayer player, String plugin) {
		this(player, plugin, ManagerLoader.getManager(AchievementManager.class).getAchievements(plugin).stream().filter(a -> a instanceof SoloAchievement)
			.map(a -> (SoloAchievement) a).collect(Collectors.toSet()));
	}

	public SoloList(WSPlayer player, String plugin, Set<SoloAchievement> achievements) {
		super(LanguagesAPI.getMessage(player, "menu.challenge.title", false, Achievements.getInstance(), "<PLUGIN>", plugin).orElse(WSText.empty()), InventoryRows.FIVE,
			1 + (achievements.size() / 21), player, "achievements_solo_list");
		WSTask.of(() -> {
			int currentSlot = 0;
			int inventory = 0;
			for (SoloAchievement achievement : achievements) {
				if (currentSlot >= slots.length) {
					currentSlot = 0;
					inventory++;
				}
				addRunnable(new RunnableSoloAchievement(player, (inventory * getRows().getSlots()) + slots[currentSlot], achievement));
				currentSlot++;
			}

			addRunnable(new RunnableSoloSummary(player, plugin, achievements));
			addHotbarRunnable(new HotbarRunnableBack(player, 8) {
				@Override
				public void onHotbarClick(MultiInventoryClickHotbarEvent event) {
					event.getPlayer().playSound(WSSound.UI_BUTTON_CLICK, 1);
					if (ManagerLoader.getManager(AchievementManager.class).getAchievements(plugin).stream().noneMatch(a -> a instanceof ProgressAchievement))
						new PluginList(getPlayer()).openFirst();
					else new PluginTypeSelector(getPlayer(), plugin).openFirst();
				}
			});
		}).runAsynchronously(Achievements.getInstance());

	}
}

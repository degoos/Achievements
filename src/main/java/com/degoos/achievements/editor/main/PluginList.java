package com.degoos.achievements.editor.main;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.Editor;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.InventoryRows;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;
import java.util.Map.Entry;

public class PluginList extends Editor {

	private static final int[] slots = new int[]{10, 11, 12, 13, 14, 15, 16, 19, 20, 21, 22, 23, 24, 25};

	public PluginList(WSPlayer player) {
		super(LanguagesAPI.getMessage(player, "menu.main.title", false, Achievements.getInstance()).orElse(WSText.empty()), InventoryRows.FOUR,
			1 + (ManagerLoader.getManager(PluginManager.class).getPlugins().size() / 14), player, "achievements_main", true);
		WSTask.of(() -> {
			AchievementManager manager = ManagerLoader.getManager(AchievementManager.class);
			int inventory = 0;
			int currentSlot = 0;
			for (Entry<String, WSItemStack> plugin : ManagerLoader.getManager(PluginManager.class).getPlugins().entrySet()) {
				if (manager.getAchievements(plugin.getKey()).isEmpty()) continue;
				if (currentSlot >= slots.length) {
					currentSlot = 0;
					inventory++;
				}
				addRunnable(new RunnablePlugin(player, (inventory * getRows().getSlots()) + slots[currentSlot], plugin.getValue(), plugin.getKey()));
				currentSlot++;
			}
		}).runAsynchronously(Achievements.getInstance());
	}
}

package com.degoos.achievements.command;

import com.degoos.achievements.editor.main.PluginList;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AchievementCommand extends WSCommand {

	public AchievementCommand() {
		super("achievements", "Achievements command", "ach");
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments) {
		if (!commandSource.hasPermission("achievements.command") || !(commandSource instanceof WSPlayer)) return;
		if (arguments.length == 0 || arguments[0].isEmpty()) new PluginList((WSPlayer) commandSource).openFirst();
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments) {
		return arguments.length == 1 ? ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().stream()
			.filter(plugin -> plugin.toLowerCase().startsWith(arguments[0].toLowerCase())).collect(Collectors.toList()) : new ArrayList<>();
	}
}

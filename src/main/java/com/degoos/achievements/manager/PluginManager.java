package com.degoos.achievements.manager;

import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.util.Validate;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class PluginManager implements Manager {

	protected Map<String, WSItemStack> plugins;

	@Override
	public void load() {
	}

	public Map<String, WSItemStack> getPlugins() {
		return new HashMap<>(plugins);
	}

	public boolean containsPlugin(String plugin) {
		return plugins.containsKey(plugin);
	}

	public boolean addPlugin(WSPlugin plugin, WSItemStack itemStack) {
		Validate.notNull(plugin, "Plugin cannot be null!");
		return addPlugin(plugin.getId(), itemStack);
	}

	public boolean addPlugin(String plugin, WSItemStack itemStack) {
		Validate.notNull(plugin, "Plugin cannot be null!");
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		if (!plugins.containsKey(plugin)) {
			plugins.put(plugin, itemStack);
			try {
				Statement statement = ManagerLoader.getManager(DatabaseManager.class).getDatabase().createStatement();
				statement.executeUpdate("ALTER TABLE achievements_players ADD " + plugin + " TEXT(100000)");
				statement.executeUpdate("INSERT INTO achievements_plugins VALUES ('" + plugin + "', '" + itemStack.toSerializedNBTTag() + "')");
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return true;
		}
		return false;
	}
}

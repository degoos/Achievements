package com.degoos.achievements;

import com.degoos.achievements.command.AchievementManagerCommand;
import com.degoos.achievements.command.AchievementCommand;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.plugin.WSPlugin;

public class Achievements extends WSPlugin {

	private static Achievements instance;

	@Override
	public void onEnable() {
		instance = this;
		ManagerLoader.load();
		WetSponge.getCommandManager().addCommand(new AchievementCommand());
		WetSponge.getCommandManager().addCommand(new AchievementManagerCommand());
	}

	public static Achievements getInstance() {
		return instance;
	}
}

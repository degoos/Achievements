package com.degoos.achievements.command.subcommand;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.api.AchievementAPI;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.util.NumericUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AMSubCommandDelete extends WSSubcommand {


	public AMSubCommandDelete(WSRamifiedCommand command) {
		super("delete", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if (remainingArguments.length < 2) {
			return;
		}
		if (!NumericUtils.isInteger(remainingArguments[1])) {
			LanguagesAPI.sendMessage(commandSource, "command.error.idMustBeInteger", Achievements.getInstance());
			return;
		}

		Achievement achievement = AchievementAPI.getManager(AchievementManager.class).getAchievement(remainingArguments[0], Integer.valueOf(remainingArguments[1]))
			.orElse(null);

		if (achievement == null) {
			LanguagesAPI.sendMessage(commandSource, "command.error.achievementDoesntExist", Achievements.getInstance());
			return;
		}

		ManagerLoader.getManager(AchievementManager.class).deleteAchievement(achievement);

		LanguagesAPI.sendMessage(commandSource, "command.done", Achievements.getInstance());
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		switch (remainingArguments.length) {
			case 1:
				return ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().stream()
					.filter(plugin -> plugin.toLowerCase().startsWith(remainingArguments[0].toLowerCase())).collect(Collectors.toList());
			default:
				return new ArrayList<>();
		}
	}
}

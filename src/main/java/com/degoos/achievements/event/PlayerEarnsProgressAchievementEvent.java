package com.degoos.achievements.event;

import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.util.Validate;

public class PlayerEarnsProgressAchievementEvent extends WSEvent {

	private APlayer aPlayer;
	private ProgressAchievement achievement;
	private int progress;

	public PlayerEarnsProgressAchievementEvent(APlayer aPlayer, ProgressAchievement achievement, int progress) {
		Validate.notNull(aPlayer, "APlayer cannot be null!");
		Validate.notNull(achievement, "Achievement cannot be null!");
		this.aPlayer = aPlayer;
		this.achievement = achievement;
		this.progress = Math.max(1, progress);
	}

	public APlayer getPlayer() {
		return aPlayer;
	}

	public ProgressAchievement getAchievement() {
		return achievement;
	}

	public int getProgress() {
		return progress;
	}

	public static class Pre extends PlayerEarnsProgressAchievementEvent implements WSCancellable {

		private boolean cancelled;

		public Pre(APlayer aPlayer, ProgressAchievement achievement, int status) {
			super(aPlayer, achievement, status);
			cancelled = false;
		}

		public void setAchievement(ProgressAchievement achievement) {
			Validate.notNull(achievement, "Achievement cannot be null!");
			super.achievement = achievement;
		}

		public void setStatus(int status) {
			super.progress = Math.max(1, status);
		}

		@Override
		public boolean isCancelled() {
			return cancelled;
		}

		@Override
		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}
	}

	public static class Post extends PlayerEarnsProgressAchievementEvent {

		public Post(APlayer aPlayer, ProgressAchievement achievement, int status) {
			super(aPlayer, achievement, status);
		}
	}
}

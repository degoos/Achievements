package com.degoos.achievements.manager;

import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.NumericUtils;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class PlayerManager implements Manager {

	private Set<APlayer> players;

	@Override
	public void load() {
		players = new HashSet<>();
	}

	public APlayer addPlayer(WSPlayer player) {
		APlayer aPlayer = addPlayer(player.getUniqueId(), player.getName());
		player.addProperty(APlayer.PROPERTY_NAME, aPlayer);
		return aPlayer;
	}

	public APlayer addPlayer(UUID uuid, String name) {
		removePlayer(uuid);
		try {
			ResultSet resultSet = ManagerLoader.getManager(DatabaseManager.class).getDatabase().createStatement()
				.executeQuery("SELECT * FROM achievements_players WHERE uuid = '" + uuid + "'");
			if (!resultSet.next()) {
				APlayer aPlayer = createNewPlayer(uuid, name);
				players.add(aPlayer);
				return aPlayer;
			}
			Map<Achievement, Integer> achievements = new HashMap<>();
			AchievementManager achievementManager = ManagerLoader.getManager(AchievementManager.class);
			ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().forEach(plugin -> {
				try {
					String list = resultSet.getString(plugin);
					if (list == null) return;
					for (String string : list.split(";")) {
						String[] sl = string.split(":");
						if (sl.length != 2 || !NumericUtils.isInteger(sl[0]) || !NumericUtils.isInteger(sl[1])) return;
						Optional<Achievement> optional = achievementManager.getAchievement(plugin, Integer.valueOf(sl[0]));
						if (!optional.isPresent()) return;
						int value = Integer.valueOf(sl[1]);
						if (value == 0) return;
						achievements.put(optional.get(), value);
					}
				} catch (Exception ex) {
					InternalLogger.printException(ex, "An error has occurred while Achievements was loading a player " + name + "!");
				}
			});
			APlayer aPlayer = new APlayer(uuid, name, achievements);
			players.add(aPlayer);
			return aPlayer;
		} catch (Exception ex) {
			InternalLogger.printException(ex, "An error has occurred while Achievements was loading a player " + name + "!");
			return null;
		}
	}

	private APlayer createNewPlayer(UUID uuid, String name) {
		APlayer aPlayer = new APlayer(uuid, name, null);
		try {
			ManagerLoader.getManager(DatabaseManager.class).getDatabase().createStatement()
				.executeUpdate("INSERT INTO achievements_players (uuid, player_name) VALUES " + "('" + uuid + "', '" + name + "')");
		} catch (SQLException ex) {
			InternalLogger.printException(ex, "An error has occurred while Achievements was creating a player " + name + "!");
		}
		return aPlayer;
	}


	public void removePlayer(WSPlayer player) {
		removePlayer(player.getUniqueId());
	}

	public void removePlayer(UUID uuid) {
		players.removeIf(aPlayer -> aPlayer.getUniqueId().equals(uuid));
	}
}

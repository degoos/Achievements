package com.degoos.achievements.editor.progress;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.Editor;
import com.degoos.achievements.editor.general.HotbarRunnableBack;
import com.degoos.achievements.editor.main.PluginList;
import com.degoos.achievements.editor.plugin.PluginTypeSelector;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.InventoryRows;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickHotbarEvent;
import com.degoos.wetsponge.sound.WSSound;
import com.degoos.wetsponge.task.WSTask;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;
import java.util.stream.Collectors;

public class ProgressList extends Editor {

	public ProgressList(WSPlayer player, String plugin) {
		this(player, plugin, ManagerLoader.getManager(AchievementManager.class).getAchievements(plugin).stream().filter(a -> a instanceof ProgressAchievement)
			.map(a -> (ProgressAchievement) a).collect(Collectors.toSet()));
	}

	public ProgressList(WSPlayer player, String plugin, Set<ProgressAchievement> achievements) {
		super(LanguagesAPI.getMessage(player, "menu.tiered.title", false, Achievements.getInstance(), "<PLUGIN>", plugin).orElse(WSText.empty()), InventoryRows.FIVE, 1 +
			(achievements.stream().mapToInt(a -> a.getPoints().size() % 5 == 0 ? a.getPoints().size() / 5 : 1 + (a.getPoints().size() / 5)).sum()) /
				7, player, "achievements_progress_list");
		WSTask.of(() -> {
			int currentSlot = 1;
			int inventory = 0;
			for (ProgressAchievement achievement : achievements) {
				if (currentSlot > 7) {
					currentSlot = 1;
					inventory++;
				}

				int phaseIndex = 0;
				for (int phase = 1; phase <= achievement.getPoints().size(); phase++) {
					if (phaseIndex > 4) {
						phaseIndex = 0;
						currentSlot++;
						if (currentSlot > 7) {
							currentSlot = 1;
							inventory++;
						}
					}
					addRunnable(new RunnableProgressAchievement(player,
						(inventory * getRows().getSlots()) + currentSlot + (36 - (9 * phaseIndex)), achievement, phase, phase == achievement.getPoints().size()));
					phaseIndex++;
				}

				currentSlot++;
			}

			addRunnable(new RunnableProgressSummary(player, plugin, achievements));
			addHotbarRunnable(new HotbarRunnableBack(player, 8) {
				@Override
				public void onHotbarClick(MultiInventoryClickHotbarEvent event) {
					event.getPlayer().playSound(WSSound.UI_BUTTON_CLICK, 1);
					if (ManagerLoader.getManager(AchievementManager.class).getAchievements(plugin).stream().noneMatch(a -> a instanceof ProgressAchievement))
						new PluginList(getPlayer()).openFirst();
					else new PluginTypeSelector(getPlayer(), plugin).openFirst();
				}
			});
		}).runAsynchronously(Achievements.getInstance());

	}
}

package com.degoos.achievements.manager;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.util.InternalLogger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DatabaseManager implements Manager {

	private Connection connection;

	@Override
	public void load() {
		try {
			getDatabase();
			Statement statement = connection.createStatement();
			statement.executeUpdate(
				"CREATE TABLE IF NOT EXISTS achievements_achievements (id INT, plugin VARCHAR(255), ac_name VARCHAR(255), description TEXT(100000), " +
					"points TEXT(100000), progress BIT , progress_required_number TEXT(100000), disabled BIT)");
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS achievements_players (uuid VARCHAR(255), player_name VARCHAR(255))");
			statement.executeUpdate("CREATE TABLE IF NOT EXISTS achievements_plugins (id VARCHAR(255), item TEXT (100000))");
			ResultSet pluginResultSet = statement.executeQuery("SELECT * FROM  achievements_plugins");
			Map<String, WSItemStack> plugins = new HashMap<>();
			while (pluginResultSet.next()) {
				try {
					plugins.put(pluginResultSet.getString("id"), WSItemStack.ofSerializedNBTTag(pluginResultSet.getString("item")));
				} catch (Exception ex) {
					InternalLogger.printException(ex, "An error has occurred while Achievements was loading a plugin item!");
					plugins.put(pluginResultSet.getString("id"), WSItemStack.of(WSBlockTypes.DIAMOND_BLOCK));
				}
			}

			Set<String> addedPlugins = new HashSet<>();
			ResultSet columns = connection.getMetaData().getColumns(null, null, "achievements_players", null);
			while (columns.next()) addedPlugins.add(columns.getString(4));
			plugins.keySet().stream().filter(plugin -> !addedPlugins.contains(plugin)).forEach(plugin -> {
				try {
					statement.executeUpdate("ALTER TABLE achievements_players ADD " + plugin + " TEXT(100000)");
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
			ManagerLoader.getManager(PluginManager.class).plugins = plugins;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void openDatabase() throws ClassNotFoundException, SQLException {
		ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getConfig();

		String driver = config.getString("database.driver");
		String url = config.getString("database.url");
		String username = config.getString("database.username");
		String password = config.getString("database.password");
		Class.forName(driver);
		boolean mySql = driver.toLowerCase().contains("mysql");
		connection = mySql ? DriverManager.getConnection(url, username, password) : DriverManager.getConnection(replaceDatabaseString(url));
	}


	public static String replaceDatabaseString(String input) {
		input = input.replaceAll("\\{DIR\\}", Achievements.getInstance().getDataFolder().getPath().replaceAll("\\\\", "/") + "/");
		input = input.replaceAll("\\{NAME\\}", Achievements.getInstance().getId().replaceAll("[^\\w_-]", ""));

		return input;
	}

	public Connection getDatabase() {
		try {
			if (connection == null || connection.isClosed()) openDatabase();
		} catch (Exception ex) {
			Achievements.getInstance().getLogger().sendError("An error has occurred white Achievements was loading the database!");
			ex.printStackTrace();
			WetSponge.getPluginManager().unloadPlugin(Achievements.getInstance());
		}
		return connection;
	}

}

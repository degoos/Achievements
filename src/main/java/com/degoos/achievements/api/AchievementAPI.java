package com.degoos.achievements.api;

import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.AchievementManager;
import com.degoos.achievements.manager.Manager;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.item.WSItemStack;
import java.util.Optional;

public class AchievementAPI {

	/**
	 * Registers a plugin. This must be the first method dependencies must call.
	 *
	 * @param plugin the plugin to register.
	 * @param pluginItemStack the displayed item stack of the plugin.
	 */
	public static void registerPlugin(String plugin, WSItemStack pluginItemStack) {
		ManagerLoader.getManager(PluginManager.class).addPlugin(plugin, pluginItemStack);
	}

	/**
	 * Registers an achievement and returns the given achievement if the achievement was not registered or the loaded achievement instance if it was already in the
	 * database.
	 *
	 * @param achievement the achievement to register.
	 *
	 * @return the given achievement if the achievement was not registered or the loaded achievement instance if it was already in the database.
	 */
	public static Achievement registerAchievement(Achievement achievement) {
		return ManagerLoader.getManager(AchievementManager.class).addAchievement(achievement);
	}

	/**
	 * Returns the {@link APlayer APlayer} instance of the given {@link WSPlayer WSPlayer}, if present.
	 *
	 * @param player the {@link WSPlayer WSPlayer}.
	 *
	 * @return the {@link APlayer APlayer}, if present.
	 */
	public static Optional<APlayer> getAPlayer(WSPlayer player) {
		return player.getProperty(APlayer.PROPERTY_NAME, APlayer.class);
	}

	/**
	 * Returns the requested Manager.
	 *
	 * @param clazz the Manager class.
	 * @param <T> the Manager.
	 *
	 * @return the Manager.
	 */
	public static <T extends Manager> T getManager(Class<T> clazz) {
		return ManagerLoader.getManager(clazz);
	}

}

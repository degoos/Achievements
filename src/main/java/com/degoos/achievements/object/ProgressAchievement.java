package com.degoos.achievements.object;

import com.degoos.wetsponge.util.Validate;
import java.util.HashMap;
import java.util.Map;

public class ProgressAchievement extends Achievement {

	private Map<Integer, Integer> required;
	private Map<Integer, Integer> points;

	public ProgressAchievement(String plugin, int id, String name, String description, boolean disabled, Map<Integer, Integer> required, Map<Integer, Integer> points) {
		super(plugin, id, name, description, disabled);
		Validate.notNull(points, "Points cannot be null!");
		Validate.isTrue(!points.isEmpty(), "Points cannot be empty!");
		Validate.notNull(required, "Phases cannot be null!");
		Validate.isTrue(!required.isEmpty(), "Phases cannot be empty!");
		this.required = required == null ? new HashMap<>() : new HashMap<>(required);
		this.points = required == null ? new HashMap<>() : new HashMap<>(points);
	}

	@Override
	public int getPoints(APlayer player) {
		if (isDisabled()) return 0;
		int phase = player.getAchievements().getOrDefault(this, 0);
		if (phase == 0) return 0;
		int points = 0;
		for (int i = 1; i <= phase; i++) {
			int phasePoints = this.points.get(i);
			if (phasePoints == 0) return points;
			points += phasePoints;
		}
		return points;
	}

	@Override
	public int getMaxPoints() {
		int i = 0;
		for (Integer point : points.values())
			i += point;
		return i;
	}

	public Map<Integer, Integer> getRequired() {
		return new HashMap<>(required);
	}

	public Map<Integer, Integer> getPoints() {
		return new HashMap<>(points);
	}

	public int getRequiredLength() {
		return required.size();
	}
}

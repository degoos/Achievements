package com.degoos.achievements.object;

import com.degoos.wetsponge.util.Validate;
import java.util.Optional;

public abstract class Achievement {

	private String plugin;
	private int id;
	private String name, description;
	private boolean disabled;

	public Achievement(String plugin, int id, String name, String description, boolean disabled) {
		Validate.notNull(plugin, "Plugin cannot be null!");
		this.plugin = plugin;
		this.id = id;
		this.name = name;
		this.description = description;
		this.disabled = disabled;
	}

	public String getPlugin() {
		return plugin;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getDescription() {
		return Optional.ofNullable(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public abstract int getPoints(APlayer player);

	public abstract int getMaxPoints();

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Achievement that = (Achievement) o;

		if (id != that.id) return false;
		return plugin.equals(that.plugin);
	}

	@Override
	public int hashCode() {
		int result = plugin.hashCode();
		result = 31 * result + id;
		return result;
	}

	@Override
	public String toString() {
		return "Achievement {id = " + id + ", plugin = " + plugin + "}";
	}
}

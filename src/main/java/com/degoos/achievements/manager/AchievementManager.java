package com.degoos.achievements.manager;

import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.wetsponge.util.NumericUtils;
import com.degoos.wetsponge.util.Validate;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class AchievementManager implements Manager {

	private Map<String, Set<Achievement>> achievements;

	@Override
	public void load() {
		achievements = new HashMap<>();
		DatabaseManager databaseManager = ManagerLoader.getManager(DatabaseManager.class);
		ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().forEach(plugin -> {
			Set<Achievement> set = new HashSet<>();
			try {
				Statement statement = databaseManager.getDatabase().createStatement();
				ResultSet result = statement.executeQuery("SELECT * FROM achievements_achievements WHERE plugin = '" + plugin + "'");
				while (result.next()) {
					if (result.getBoolean("progress")) {
						set.add(new ProgressAchievement(plugin, result.getInt("id"), result.getString("ac_name"), result.getString("description"), result
							.getBoolean("disabled"), getIntegerMap(result.getString("progress_required_number")), getIntegerMap(result.getString("points"))));
					} else set.add(new SoloAchievement(plugin, result.getInt("id"), result.getString("ac_name"), result.getString("description"), result
						.getBoolean("disabled"), Integer.valueOf(result.getString("points"))));
				}
				achievements.put(plugin, set);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		});
	}

	private Map<Integer, Integer> getIntegerMap(String string) {
		String[] sl = string.split(";");
		Map<Integer, Integer> map = new HashMap<>();
		for (String s : sl) {
			String[] sll = s.split(":");
			if (sll.length != 2 || !NumericUtils.isInteger(sll[0]) || !NumericUtils.isInteger(sll[1])) continue;
			map.put(Integer.valueOf(sll[0]), Integer.valueOf(sll[1]));
		}
		return map;
	}

	public Map<String, Set<Achievement>> getAchievements() {
		Map<String, Set<Achievement>> map = new HashMap<>();
		achievements.keySet().forEach(plugin -> map.put(plugin, getAchievements(plugin)));
		return map;
	}

	public Set<Achievement> getAchievements(String plugin) {
		return achievements.containsKey(plugin) ? achievements.get(plugin).stream().filter(achievement -> !achievement.isDisabled()).collect(Collectors.toSet())
		                                        : new HashSet<>();
	}

	public Optional<Achievement> getAchievement(String plugin, int id) {
		return achievements.containsKey(plugin) ? achievements.get(plugin).stream().filter(target -> !target.isDisabled() && target.getId() == id).findAny()
		                                        : Optional.empty();
	}

	public Achievement addAchievement(Achievement achievement) {
		Validate.notNull(achievement, "Achievement cannot be null!");
		Set<Achievement> achievements;
		if (!this.achievements.containsKey(achievement.getPlugin())) {
			achievements = new HashSet<>();
			achievements.add(achievement);
			this.achievements.put(achievement.getPlugin(), achievements);
			addAchievementToDatabase(achievement);
			return achievement;
		} else {
			achievements = this.achievements.get(achievement.getPlugin());
			Optional<Achievement> optional = achievements.stream().filter(target -> target.getId() == achievement.getId()).findAny();
			if (optional.isPresent()) return optional.get();
			achievements.add(achievement);
			addAchievementToDatabase(achievement);
			return achievement;
		}
	}

	private void addAchievementToDatabase(Achievement achievement) {
		try {
			StringBuilder points;
			StringBuilder required;
			if (achievement instanceof SoloAchievement) {
				points = new StringBuilder(String.valueOf(achievement.getMaxPoints()));
				required = new StringBuilder();
			} else if (achievement instanceof ProgressAchievement) {
				points = new StringBuilder();
				for (Entry<Integer, Integer> entry : ((ProgressAchievement) achievement).getPoints().entrySet())
					points.append(";").append(entry.getKey()).append(":").append(entry.getValue());
				points = new StringBuilder(points.toString().replaceFirst(";", ""));
				required = new StringBuilder();
				for (Entry<Integer, Integer> entry : ((ProgressAchievement) achievement).getRequired().entrySet())
					required.append(";").append(entry.getKey()).append(":").append(entry.getValue());
				required = new StringBuilder(required.toString().replaceFirst(";", ""));
			} else {
				points = new StringBuilder("0");
				required = new StringBuilder();
			}

			PreparedStatement statement = ManagerLoader.getManager(DatabaseManager.class).getDatabase().prepareStatement(
				"INSERT INTO achievements_achievements (id, plugin, ac_name, description, points, progress, progress_required_number, disabled) VALUES (?, ?, ?," +
					" ?, ?, ?, ?, ?)");
			statement.setInt(1, achievement.getId());
			statement.setString(2, achievement.getPlugin());
			statement.setString(3, achievement.getName());
			statement.setString(4, achievement.getDescription().orElse(null));
			statement.setString(5, points.toString());
			statement.setBoolean(6, achievement instanceof ProgressAchievement);
			statement.setString(7, required.toString());
			statement.setBoolean(8, achievement.isDisabled());
			statement.execute();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void deleteAchievement(Achievement achievement) {
		Validate.notNull(achievement, "Achievement cannot be null!");
		if (!this.achievements.containsKey(achievement.getPlugin())) return;
		Set<Achievement> set = this.achievements.get(achievement.getPlugin());
		if (set.removeIf(target -> target.getId() == achievement.getId())) {
			removeAchievementFromDatabase(achievement);
			if (set.isEmpty()) this.achievements.remove(achievement.getPlugin());
		}
	}


	private void removeAchievementFromDatabase(Achievement achievement) {
		try {
			PreparedStatement statement = ManagerLoader.getManager(DatabaseManager.class).getDatabase()
				.prepareStatement("DELETE FROM achievements_achievements WHERE id = ? AND plugin = ? ");
			statement.setInt(1, achievement.getId());
			statement.setString(2, achievement.getPlugin());
			statement.execute();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

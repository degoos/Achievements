package com.degoos.achievements.object;

public class SoloAchievement extends Achievement {

	private int points;

	public SoloAchievement(String plugin, int id, String name, String description, boolean disabled, int points) {
		super(plugin, id, name, description, disabled);
		this.points = points;
	}

	@Override
	public int getPoints(APlayer player) {
		return !isDisabled() && player.getAchievements().containsKey(this) ? points : 0;
	}

	@Override
	public int getMaxPoints() {
		return points;
	}
}

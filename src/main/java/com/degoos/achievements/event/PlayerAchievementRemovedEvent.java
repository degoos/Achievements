package com.degoos.achievements.event;

import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;
import com.degoos.wetsponge.util.Validate;

public class PlayerAchievementRemovedEvent extends WSEvent {

	private APlayer aPlayer;
	private Achievement achievement;

	public PlayerAchievementRemovedEvent(APlayer aPlayer, Achievement achievement) {
		Validate.notNull(aPlayer, "APlayer cannot be null!");
		Validate.notNull(achievement, "Achievement cannot be null!");
		this.aPlayer = aPlayer;
		this.achievement = achievement;
	}

	public APlayer getPlayer() {
		return aPlayer;
	}

	public Achievement getAchievement() {
		return achievement;
	}

	public static class Pre extends PlayerAchievementRemovedEvent implements WSCancellable {

		private boolean cancelled;

		public Pre(APlayer aPlayer, Achievement achievement) {
			super(aPlayer, achievement);
			cancelled = false;
		}

		public void setAchievement(Achievement achievement) {
			Validate.notNull(achievement, "Achievement cannot be null!");
			super.achievement = achievement;
		}

		@Override
		public boolean isCancelled() {
			return cancelled;
		}

		@Override
		public void setCancelled(boolean cancelled) {
			this.cancelled = cancelled;
		}
	}

	public static class Post extends PlayerAchievementRemovedEvent {

		public Post(APlayer aPlayer, Achievement achievement) {
			super(aPlayer, achievement);
		}
	}
}

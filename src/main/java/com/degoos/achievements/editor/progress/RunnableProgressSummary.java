package com.degoos.achievements.editor.progress;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorHotbarRunnable;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.Achievement;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.achievements.object.SoloAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickHotbarEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.text.WSText;
import java.util.Set;

public class RunnableProgressSummary extends EditorHotbarRunnable {

	public RunnableProgressSummary(WSPlayer player, String plugin, Set<ProgressAchievement> achievements) {
		super(player, 4, WSItemStack.of(WSBlockTypes.STONE));

		WSItemStack item = ManagerLoader.getManager(PluginManager.class).getPlugins().get(plugin);
		if (item == null) return;

		APlayer aPlayer = player.getProperty(APlayer.PROPERTY_NAME, APlayer.class).orElse(null);
		if (aPlayer == null) return;

		int maxAchievements = achievements.stream().mapToInt(ProgressAchievement::getRequiredLength).sum();
		int unlocked = achievements.stream().mapToInt(aPlayer::getAchievementPhase).sum();

		int maxPoints = achievements.stream().mapToInt(Achievement::getMaxPoints).sum();
		int points = achievements.stream().mapToInt(achievement -> achievement.getPoints(aPlayer)).sum();

		int unlockPer = maxAchievements == 0 ? 100 : unlocked * 100 / maxAchievements;
		int pointsPer = maxPoints == 0 ? 100 : points * 100 / maxPoints;

		StringBuilder greenBar = new StringBuilder();
		StringBuilder grayBar = new StringBuilder();
		for (int i = 5; i <= 100; i += 5) {
			if (i <= unlockPer) greenBar.append("■");
			else grayBar.append("■");
		}
		WSText unlockBar = WSText.of(greenBar.toString(), EnumTextColor.GREEN, WSText.of(grayBar.toString(), EnumTextColor.DARK_GRAY));

		greenBar = new StringBuilder();
		grayBar = new StringBuilder();
		for (int i = 5; i <= 100; i += 5) {
			if (i <= pointsPer) greenBar.append("■");
			else grayBar.append("■");
		}
		WSText pointsBar = WSText.of(greenBar.toString(), EnumTextColor.GREEN, WSText.of(grayBar.toString(), EnumTextColor.DARK_GRAY));

		item.setDisplayName(LanguagesAPI.getMessage(player, "menu.type.tiered.title", false, Achievements.getInstance(), "<PLUGIN>", plugin).orElse(WSText.empty()));
		item.setLore(LanguagesAPI.getMessages(player, "menu.type.tiered.loreNoAction", false, Achievements
			.getInstance(), "<PLUGIN>", plugin, "<UNLOCKED>", unlocked, "<ACHIEVEMENTS>", maxAchievements, "<UNLOCKED_PER>", unlockPer, "<UNLOCK_BAR>", unlockBar,
			"<POINTS>", points, "<MAX_POINTS>", maxPoints, "<POINTS_PER>", pointsPer, "<POINTS_BAR>", pointsBar));

		item.update();
		setItemStack(item);
	}

	@Override
	public void onHotbarClick(MultiInventoryClickHotbarEvent event) {

	}
}

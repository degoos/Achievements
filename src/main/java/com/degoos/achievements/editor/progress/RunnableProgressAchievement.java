package com.degoos.achievements.editor.progress;

import com.degoos.achievements.Achievements;
import com.degoos.achievements.editor.EditorRunnable;
import com.degoos.achievements.manager.MessageManager;
import com.degoos.achievements.object.APlayer;
import com.degoos.achievements.object.ProgressAchievement;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.enums.EnumDyeColor;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.blockType.WSBlockTypeDyeColor;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.material.itemType.WSItemTypes;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.NumericUtils;

public class RunnableProgressAchievement extends EditorRunnable {

	private static final WSItemStack DIAMOND_HEAD = WSItemStack.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQ" +
		"ubmV0L3RleHR1cmUvOTYzMTU5N2RjZTRlNDA1MWU4ZDVhNTQzNjQxOTY2YWI1NGZiZjI1YTBlZDYwNDdmMTFlNjE0MGQ4OGJmNDhmIn19fQ==");
	private static final WSItemStack COAL_HEAD = WSItemStack.createSkull("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3" +
		"RleHR1cmUvZjZjNWVjYWM5NDJjNzdiOTVhYjQ2MjBkZjViODVlMzgwNjRjOTc0ZjljNWM1NzZiODQzNjIyODA2YTQ1NTcifX19");

	public RunnableProgressAchievement(WSPlayer player, int slot, ProgressAchievement achievement, int phase, boolean finalPhase) {
		super(player, slot, WSItemStack.of(WSItemTypes.COAL));

		APlayer aPlayer = player.getProperty(APlayer.PROPERTY_NAME, APlayer.class).orElse(null);
		if (aPlayer == null) return;
		boolean hasAchievement = aPlayer.getAchievementPhase(achievement) >= phase;
		WSItemStack itemStack = finalPhase ? hasAchievement ? DIAMOND_HEAD.clone() : COAL_HEAD.clone() : WSItemStack
			.of(((WSBlockTypeDyeColor) WSBlockTypes.STAINED_GLASS_PANE.getDefaultType()).setDyeColor(hasAchievement ? EnumDyeColor.CYAN : EnumDyeColor.GRAY));
		itemStack.setDisplayName(LanguagesAPI.getMessage(player,
			"menu.tiered.achievement." + (hasAchievement ? "unlockedTitle" : "lockedTitle"), false, Achievements.getInstance(), "<NAME>", achievement
				.getName(), "<PHASE>", NumericUtils.toRomanNumeral(phase)).orElse(WSText.empty()));
		itemStack.setLore(MessageManager.getItemProgressDescription(player, achievement, phase, hasAchievement));
		itemStack.setQuantity(Math.min(64, Math.max(achievement.getMaxPoints(), 1)));
		setItemStack(itemStack.update());
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {

	}
}

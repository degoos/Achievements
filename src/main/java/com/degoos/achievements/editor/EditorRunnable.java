package com.degoos.achievements.editor;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.util.Validate;

public abstract class EditorRunnable {

	private int slot;
	private WSItemStack itemStack;
	private WSPlayer player;
	private Editor editor;

	public EditorRunnable(WSPlayer player, int slot, WSItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.slot = slot;
		this.itemStack = itemStack.clone();
		this.player = player;
	}


	public int getSlot() {
		return slot;
	}

	public WSItemStack getItemStack() {
		return itemStack.clone();
	}

	public void setItemStack(WSItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack.clone().setQuantity(1).update();
		if (editor != null) editor.setItem(slot, itemStack);
	}

	protected void setItemStackWithoutUpdating(WSItemStack itemStack) {
		Validate.notNull(itemStack, "ItemStack cannot be null!");
		this.itemStack = itemStack;
	}

	public WSPlayer getPlayer() {
		return player;
	}

	public Editor getEditor() {
		return editor;
	}

	public void setEditor(Editor editor) {
		this.editor = editor;
	}

	public abstract void onClick(MultiInventoryClickEvent event);

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		EditorRunnable that = (EditorRunnable) o;

		return slot == that.slot;
	}

	@Override
	public int hashCode() {
		return slot;
	}
}

package com.degoos.achievements.object;

import com.degoos.achievements.event.PlayerAchievementRemovedEvent;
import com.degoos.achievements.event.PlayerEarnsProgressAchievementEvent;
import com.degoos.achievements.event.PlayerEarnsSoloAchievementEvent;
import com.degoos.achievements.loader.ManagerLoader;
import com.degoos.achievements.manager.DatabaseManager;
import com.degoos.achievements.manager.MessageManager;
import com.degoos.achievements.manager.PluginManager;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.util.InternalLogger;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class APlayer {

	public static final String PROPERTY_NAME = "achievements_player";

	private UUID uniqueId;
	private String playerName;
	private Map<Achievement, Integer> achievements;

	public APlayer(UUID uniqueId, String playerName, Map<Achievement, Integer> achievements) {
		this.uniqueId = uniqueId;
		this.playerName = playerName;
		this.achievements = achievements == null ? new HashMap<>() : achievements;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public Map<Achievement, Integer> getAchievements() {
		return new HashMap<>(achievements);
	}

	public int getAchievementPhase(Achievement achievement) {
		return achievements.getOrDefault(achievement, 0);
	}

	public void earnAchievement(SoloAchievement achievement) {
		if (achievement.isDisabled()) return;
		if (achievements.containsKey(achievement)) return;

		PlayerEarnsSoloAchievementEvent.Pre pre = new PlayerEarnsSoloAchievementEvent.Pre(this, achievement);
		WetSponge.getEventManager().callEvent(pre);
		SoloAchievement postAchievement = pre.getAchievement();
		if (pre.isCancelled()) return;

		if (postAchievement.isDisabled() || achievements.containsKey(achievement)) return;

		WetSponge.getServer().getPlayer(uniqueId).ifPresent(player -> MessageManager.getAchievementMessage(player, postAchievement, 1).forEach(player::sendMessage));
		achievements.put(postAchievement, 1);
		updateAchievements(postAchievement.getPlugin());
		WetSponge.getEventManager().callEvent(new PlayerEarnsSoloAchievementEvent.Post(this, postAchievement));
	}

	public void earnAchievement(ProgressAchievement achievement, int progress) {
		if (achievement.isDisabled() || progress < 1) return;
		if (achievements.containsKey(achievement) && achievements.get(achievement) >= progress) return;

		PlayerEarnsProgressAchievementEvent.Pre pre = new PlayerEarnsProgressAchievementEvent.Pre(this, achievement, progress);
		WetSponge.getEventManager().callEvent(pre);
		ProgressAchievement postAchievement = pre.getAchievement();
		if (pre.isCancelled()) return;

		if (postAchievement.isDisabled() || (achievements.containsKey(postAchievement) && achievements.get(postAchievement) >= progress)) return;

		WetSponge.getServer().getPlayer(uniqueId)
			.ifPresent(player -> MessageManager.getAchievementMessage(player, postAchievement, pre.getProgress()).forEach(player::sendMessage));
		achievements.put(postAchievement, pre.getProgress());
		updateAchievements(postAchievement.getPlugin());
		WetSponge.getEventManager().callEvent(new PlayerEarnsProgressAchievementEvent.Post(this, postAchievement, pre.getProgress()));
	}

	public void earnAchievementByRequired(ProgressAchievement achievement, int required) {
		int last = 0;
		Map<Integer, Integer> requiredMap = achievement.getRequired();
		for (int i = 1; i < requiredMap.size(); i++) {
			if (requiredMap.getOrDefault(i, 0) < required) last = i;
			else {
				earnAchievement(achievement, last);
				return;
			}
		}
	}

	public void removeAchievement(Achievement achievement) {
		if (achievement.isDisabled()) return;
		if (!achievements.containsKey(achievement)) return;

		PlayerAchievementRemovedEvent.Pre pre = new PlayerAchievementRemovedEvent.Pre(this, achievement);
		WetSponge.getEventManager().callEvent(pre);
		Achievement postAchievement = pre.getAchievement();
		if (pre.isCancelled() || postAchievement.isDisabled() || !achievements.containsKey(postAchievement)) return;

		achievements.remove(postAchievement);
		updateAchievements(postAchievement.getPlugin());
		WetSponge.getEventManager().callEvent(new PlayerAchievementRemovedEvent.Post(this, postAchievement));
	}

	public void updateAchievements() {
		try {
			Statement statement = ManagerLoader.getManager(DatabaseManager.class).getDatabase().createStatement();
			ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().forEach(plugin -> updateAchievements(plugin, statement));
		} catch (SQLException ex) {
			InternalLogger.printException(ex, "An error has occurred while Achievements was updating a player!");
		}
	}

	public void updateAchievements(Statement statement) {
		ManagerLoader.getManager(PluginManager.class).getPlugins().keySet().forEach(plugin -> updateAchievements(plugin, statement));
	}

	public void updateAchievements(String plugin) {
		try {
			updateAchievements(plugin, ManagerLoader.getManager(DatabaseManager.class).getDatabase().createStatement());
		} catch (SQLException ex) {
			InternalLogger.printException(ex, "An error has occurred while Achievements was updating a player!");
		}
	}

	public void updateAchievements(String plugin, Statement statement) {
		StringBuilder builder = new StringBuilder();
		achievements.forEach((achievement, integer) -> {
			if (integer == 0 || !achievement.getPlugin().equals(plugin)) return;
			builder.append(";").append(achievement.getId()).append(":").append(integer);
		});
		try {
			statement.executeUpdate(
				"UPDATE achievements_players SET " + plugin + " = '" + builder.toString().replaceFirst(";", "") + "' WHERE uuid = '" + uniqueId.toString() + "'");
		} catch (SQLException ex) {
			InternalLogger.printException(ex, "An error has occurred while Achievements was updating a player!");
		}
	}
}

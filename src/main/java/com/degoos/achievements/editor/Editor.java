package com.degoos.achievements.editor;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.inventory.multiinventory.InventoryRows;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickEvent;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickHotbarEvent;
import com.degoos.wetsponge.inventory.multiinventory.MultiInventoryClickPlayerInvEvent;
import com.degoos.wetsponge.inventory.multiinventory.PlayerMultiInventory;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.material.blockType.WSBlockTypes;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.Validate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class Editor extends PlayerMultiInventory {

	private Set<EditorRunnable> runnables;
	private Set<EditorHotbarRunnable> hotbarRunnables;

	public Editor(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id, EditorRunnable... runnables) {
		this(name, rows, invAmount, player, id, true, runnables);
	}

	public Editor(WSText name, InventoryRows rows, int invAmount, WSPlayer player, String id, boolean forceHotbar, EditorRunnable... runnables) {
		super(name, rows, invAmount, player, "eggwars_editor_" + id, forceHotbar);
		this.runnables = Arrays.stream(runnables).filter(runnable -> !(runnable instanceof EditorHotbarRunnable)).filter(Objects::nonNull).collect(Collectors.toSet());
		this.hotbarRunnables = Arrays.stream(runnables).filter(runnable -> runnable instanceof EditorHotbarRunnable).map(runnable -> (EditorHotbarRunnable) runnable)
			.filter(Objects::nonNull).collect(Collectors.toSet());
		this.runnables.forEach(runnable -> runnable.setEditor(this));
		this.hotbarRunnables.forEach(runnable -> runnable.setEditor(this));
		setupItemStacks();
	}

	public Editor(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, EditorRunnable... runnables) {
		this(name, slots, rows, player, id, true, runnables);
	}


	public Editor(WSText name, int slots, InventoryRows rows, WSPlayer player, String id, boolean forceHotbar, EditorRunnable... runnables) {
		super(name, slots, rows, player, "eggwars_editor_" + id, forceHotbar);
		this.runnables = Arrays.stream(runnables).filter(runnable -> !(runnable instanceof EditorHotbarRunnable)).filter(Objects::nonNull).collect(Collectors.toSet());
		this.hotbarRunnables = Arrays.stream(runnables).filter(runnable -> runnable instanceof EditorHotbarRunnable).map(runnable -> (EditorHotbarRunnable) runnable)
			.filter(Objects::nonNull).collect(Collectors.toSet());
		this.runnables.forEach(runnable -> runnable.setEditor(this));
		this.hotbarRunnables.forEach(runnable -> runnable.setEditor(this));
		setupItemStacks();
	}

	public Set<EditorRunnable> getRunnables() {
		return new HashSet<>(runnables);
	}

	public void addRunnable(EditorRunnable runnable) {
		Validate.notNull(runnable, "Runnable cannot be null!");
		if (runnable instanceof EditorHotbarRunnable) addHotbarRunnable((EditorHotbarRunnable) runnable);
		else {
			if (runnables.contains(runnable)) runnables.remove(runnable);
			runnable.setEditor(this);
			runnables.add(runnable);
			setItem(runnable.getSlot(), runnable.getItemStack());
		}
	}

	public void removeRunnable(int slot) {
		new HashSet<>(runnables).stream().filter(runnable -> runnable.getSlot() == slot).forEach(runnable -> runnables.remove(runnable));
		setItem(slot, WSItemStack.of(WSBlockTypes.AIR));
	}

	public void clearRunnables() {
		runnables.forEach(runnable -> setItem(runnable.getSlot(), WSItemStack.of(WSBlockTypes.AIR)));
		runnables.clear();
	}

	public Set<EditorHotbarRunnable> getHotbarRunnables() {
		return new HashSet<>(hotbarRunnables);
	}

	public void addHotbarRunnable(EditorHotbarRunnable runnable) {
		Validate.notNull(runnable, "Runnable cannot be null!");
		if (runnables.contains(runnable)) runnables.remove(runnable);
		runnable.setEditor(this);
		hotbarRunnables.add(runnable);
		setItemOnHotbar(runnable.getSlot(), runnable.getItemStack());
	}

	public void removeHotbarRunnable(int slot) {
		new HashSet<>(hotbarRunnables).stream().filter(runnable -> runnable.getSlot() == slot).forEach(runnable -> runnables.remove(runnable));
		setItem(slot, WSItemStack.of(WSBlockTypes.AIR));
	}

	public void clearHotbarRunnables() {
		hotbarRunnables.forEach(runnable -> setItem(runnable.getSlot(), WSItemStack.of(WSBlockTypes.AIR)));
		hotbarRunnables.clear();
	}

	@Override
	public void onClick(MultiInventoryClickEvent event) {
		event.setCancelled(true);
		new HashSet<>(runnables).stream().filter(Objects::nonNull).filter(runnable -> event.getClickedMultiInventorySlot() == runnable.getSlot()).forEach(runnable -> {
			runnable.onClick(event);
		});
	}

	@Override
	public void onHotbarClick(MultiInventoryClickHotbarEvent event) {
		if (event.getHotbarSlot() == 2 || event.getHotbarSlot() == 3 || event.getHotbarSlot() == 5 || event.getHotbarSlot() == 6) return;
		event.setCancelled(true);
		new HashSet<>(hotbarRunnables).stream().filter(Objects::nonNull).filter(runnable -> event.getHotbarSlot() == runnable.getSlot()).forEach(runnable -> {
			runnable.onHotbarClick(event);
		});
	}

	@Override
	public void onPlayerInventoryClick(MultiInventoryClickPlayerInvEvent event) {
		event.setCancelled(true);
	}

	private void setupItemStacks() {
		runnables.forEach(runnable -> setItem(runnable.getSlot(), runnable.getItemStack()));
		hotbarRunnables.forEach(runnable -> setItemOnHotbar(runnable.getSlot(), runnable.getItemStack()));
	}
}
